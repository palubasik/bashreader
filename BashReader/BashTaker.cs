using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Collections;
using System.Windows.Forms;
using RssProcessing;
using LogLib;

namespace BashReader
{
    class BashTaker
    {
       
        private XmlDocument xmlBashPage;
        private RssProcessor rssBashProcessor;
 
        public BashTaker(){
            
        }
        private ArrayList arrBashJokes;
        public bool ProcessBash(){
 
            bool bOK = false;
            if (xmlBashPage != null)
            {
                rssBashProcessor = new RssProcessor();
                rssBashProcessor.ProcessRss(xmlBashPage);
                arrBashJokes = rssBashProcessor.RssDataNodes;
                bOK = true;
            }

            return bOK;
        }
        
        public bool GetBashChanges(){

            bool fOk = false;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(BashConfigReader.Config.Address);
                if (BashConfigReader.Config.Proxy.Using == true)
                {
                    WebProxy wp = new WebProxy();
                    wp.Address = new Uri(BashConfigReader.Config.Proxy.Address);

                    if (BashConfigReader.Config.Proxy.IsAuth)
                    {
                        wp.Credentials = new NetworkCredential(BashConfigReader.Config.Proxy.Login,
                         BashConfigReader.Config.Proxy.Password);
                        request.Proxy = wp;
                    }
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1251);
                StreamReader reader = new StreamReader(response.GetResponseStream(), enc);
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(reader.ReadToEnd());
                if (xml != null)
                {
                    RssProcessor rssBP = new RssProcessor();
                    rssBP.ProcessRss(xml);

                    ArrayList arr = rssBP.RssDataNodes;
                    if (arr == null || arr.Count < 1)
                    {
                        fOk = false;
                    }
                    else
                    {
                        RssNode rssn = (RssNode)arr[0];
                        if (arrBashJokes == null || arrBashJokes.Count < 1)
                        {
                            fOk = true;
                            arrBashJokes = arr;
                        }
                        else
                            if (rssn.guid != ((RssNode)arrBashJokes[0]).guid)
                            {
                                fOk = true;
                                arrBashJokes = arr;
                            }
                    }
                }
                reader.Close();
                response.Close();
            }
            catch (Exception e)
            {
#if DEBUG
                MessageBox.Show(e.ToString());
#endif
                Logger.WriteError(e);
            }
            return fOk;
        }
        public bool InitBashResponse()
        {

            bool bOK = false;
            
            try{
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(BashConfigReader.Config.Address);
                if (BashConfigReader.Config.Proxy.Using == true)
                {
                    WebProxy wp = new WebProxy();
                    wp.Address = new Uri(BashConfigReader.Config.Proxy.Address);

                    if (BashConfigReader.Config.Proxy.IsAuth)
                    {
                        wp.Credentials = new NetworkCredential(BashConfigReader.Config.Proxy.Login,
                         BashConfigReader.Config.Proxy.Password);
                        request.Proxy = wp;
                    }
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1251);
                StreamReader reader = new StreamReader(response.GetResponseStream(),enc);
                xmlBashPage = new XmlDocument();
                string sss = reader.ReadToEnd();
                xmlBashPage.LoadXml(sss);
                bOK = true;
                reader.Close();
                response.Close();
            }catch(Exception e){
#if DEBUG
                MessageBox.Show(e.ToString());
#endif
                Logger.WriteError(e);
            }

            return bOK;
        }

#region Properties
       

        public ArrayList BashJokes
        {
            get { return arrBashJokes; }
            set{}
        }
#endregion


    }
}
