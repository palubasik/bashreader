using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
namespace Constants
{
    static class VersionInfo
    {
        public const string APP_NAME = "�����������";
        public const string APP_VER = "1.0.6";
        public const string APP_NAME_VER = "����������� 1.0.6";
        public static string VersionsInfoFile = Application.StartupPath + "\\Versions.txt";
    }

    static class Status
    {
        public static class StatusText
        {
            public const string Processing = "���������/�������� ������...";
            public const string Done = "������";
        }
        public static class StatusImage
        {
            public static Bitmap Processing = BashReader.Properties.Resources.Processing;
            public static Bitmap Done = BashReader.Properties.Resources.OK;
        }
    }

    static class ConfigParams {
        public const string BashCheckingInterval = "IntervalOfBashChecking";
        public const string RandomShowingInterval = "IntervalOfRandowShowing";
        public const string EnableCheckBash = "EnableCheckBash";
        public const string EnableShowRandom = "EnableShowRandom";
    }

    static class ZoomFactor {
        public const float ZoomDelta = 0.1F;
        public const float ZoomMax = 10F;
        public const float ZoomMin = 0.3F;
    }

    static class Opacity {
        public const double OpacityMax = 1F;
        public const double OpacityMin = 0.5F;
        public const double OpacityDelta = 0.05F;
    }

    static class AutoExecConstants{
        public static string SystemPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Startup)+@"\";//@"c:\Documents and Settings\All Users\Start Menu\Programs\Startup\";
        public const string AppName = VersionInfo.APP_NAME + ".lnk";
    };

    static class ExternalFiles{
        public static string LogFile = Application.StartupPath + "\\Notifications.log";
        public static string ConfigFile = Application.StartupPath + "\\Config.cfg";
        public static string VersionsInfoFile = Application.StartupPath + "\\Versions.txt";
    };
}
