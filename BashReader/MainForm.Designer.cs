using System.Drawing;
namespace BashReader
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.JokesViewPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.contextMenuTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemShowForm = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemShowRandom = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemExitApp = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemSaveToFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemShowAll = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemLast = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemHide = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.����ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemCheckBash = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.SaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.���������ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAutoExec = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AuthorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.VersionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.timerGetBash = new System.Windows.Forms.Timer(this.components);
            this.timerPushRandom = new System.Windows.Forms.Timer(this.components);
            this.timerShowEffect = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.backgroundWorkerInitBash = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerCheckBash = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerShowEffect = new System.ComponentModel.BackgroundWorker();
            this.richTextBoxOutput = new UserControls.RichTextBoxEx();
            this.JokesViewPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.contextMenuTray.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // JokesViewPanel
            // 
            this.JokesViewPanel.Controls.Add(this.panel1);
            this.JokesViewPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.JokesViewPanel.Location = new System.Drawing.Point(0, 0);
            this.JokesViewPanel.Name = "JokesViewPanel";
            this.JokesViewPanel.Size = new System.Drawing.Size(644, 444);
            this.JokesViewPanel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(644, 444);
            this.panel1.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.richTextBoxOutput);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(644, 420);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bash.org.ru";
            // 
            // contextMenuTray
            // 
            this.contextMenuTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemShowForm,
            this.toolStripSeparator3,
            this.toolStripMenuItemShowRandom,
            this.toolStripMenuItemExitApp});
            this.contextMenuTray.Name = "contextMenuTray";
            this.contextMenuTray.Size = new System.Drawing.Size(219, 76);
            // 
            // toolStripMenuItemShowForm
            // 
            this.toolStripMenuItemShowForm.Name = "toolStripMenuItemShowForm";
            this.toolStripMenuItemShowForm.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemShowForm.Text = "�������� ���� ���������";
            this.toolStripMenuItemShowForm.Click += new System.EventHandler(this.toolStripMenuItemShow_Click_1);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(215, 6);
            // 
            // toolStripMenuItemShowRandom
            // 
            this.toolStripMenuItemShowRandom.Checked = true;
            this.toolStripMenuItemShowRandom.CheckOnClick = true;
            this.toolStripMenuItemShowRandom.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItemShowRandom.Name = "toolStripMenuItemShowRandom";
            this.toolStripMenuItemShowRandom.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemShowRandom.Text = "���������� ���������";
            this.toolStripMenuItemShowRandom.Click += new System.EventHandler(this.toolStripMenuItemShowRandom_Click);
            // 
            // toolStripMenuItemExitApp
            // 
            this.toolStripMenuItemExitApp.Name = "toolStripMenuItemExitApp";
            this.toolStripMenuItemExitApp.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemExitApp.Text = "�����";
            this.toolStripMenuItemExitApp.Click += new System.EventHandler(this.toolStripMenuItemExitApp_Click);
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemSaveToFile,
            this.toolStripSeparator5,
            this.toolStripMenuItemShowAll,
            this.toolStripMenuItemLast,
            this.toolStripSeparator4,
            this.toolStripMenuItemSetup,
            this.toolStripSeparator2,
            this.toolStripMenuItemHide,
            this.toolStripSeparator1,
            this.toolStripMenuItemExit});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(197, 160);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            // 
            // toolStripMenuItemSaveToFile
            // 
            this.toolStripMenuItemSaveToFile.Name = "toolStripMenuItemSaveToFile";
            this.toolStripMenuItemSaveToFile.Size = new System.Drawing.Size(196, 22);
            this.toolStripMenuItemSaveToFile.Text = "���������� (Ctrl + C)";
            this.toolStripMenuItemSaveToFile.Click += new System.EventHandler(this.toolStripMenuItemSaveToFile_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(193, 6);
            // 
            // toolStripMenuItemShowAll
            // 
            this.toolStripMenuItemShowAll.Name = "toolStripMenuItemShowAll";
            this.toolStripMenuItemShowAll.Size = new System.Drawing.Size(196, 22);
            this.toolStripMenuItemShowAll.Text = "�������� ���";
            this.toolStripMenuItemShowAll.Click += new System.EventHandler(this.toolStripMenuItemShowAll_Click);
            // 
            // toolStripMenuItemLast
            // 
            this.toolStripMenuItemLast.Name = "toolStripMenuItemLast";
            this.toolStripMenuItemLast.Size = new System.Drawing.Size(196, 22);
            this.toolStripMenuItemLast.Text = "�������� ���������";
            this.toolStripMenuItemLast.Click += new System.EventHandler(this.toolStripMenuItemLast_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(193, 6);
            // 
            // toolStripMenuItemSetup
            // 
            this.toolStripMenuItemSetup.Name = "toolStripMenuItemSetup";
            this.toolStripMenuItemSetup.Size = new System.Drawing.Size(196, 22);
            this.toolStripMenuItemSetup.Text = "���������";
            this.toolStripMenuItemSetup.Click += new System.EventHandler(this.toolStripMenuItemSetup_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(193, 6);
            // 
            // toolStripMenuItemHide
            // 
            this.toolStripMenuItemHide.Name = "toolStripMenuItemHide";
            this.toolStripMenuItemHide.Size = new System.Drawing.Size(196, 22);
            this.toolStripMenuItemHide.Text = "������ ����";
            this.toolStripMenuItemHide.Click += new System.EventHandler(this.toolStripMenuItemShow_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(193, 6);
            // 
            // toolStripMenuItemExit
            // 
            this.toolStripMenuItemExit.Name = "toolStripMenuItemExit";
            this.toolStripMenuItemExit.Size = new System.Drawing.Size(196, 22);
            this.toolStripMenuItemExit.Text = "�����";
            this.toolStripMenuItemExit.Click += new System.EventHandler(this.toolStripMenuItemExit_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.����ToolStripMenuItem,
            this.���������ToolStripMenuItem,
            this.AboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(644, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ����ToolStripMenuItem
            // 
            this.����ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemCheckBash,
            this.toolStripSeparator6,
            this.SaveToolStripMenuItem,
            this.ExitToolStripMenuItem});
            this.����ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("����ToolStripMenuItem.Image")));
            this.����ToolStripMenuItem.Name = "����ToolStripMenuItem";
            this.����ToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.����ToolStripMenuItem.Text = "&����";
            // 
            // toolStripMenuItemCheckBash
            // 
            this.toolStripMenuItemCheckBash.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemCheckBash.Image")));
            this.toolStripMenuItemCheckBash.Name = "toolStripMenuItemCheckBash";
            this.toolStripMenuItemCheckBash.Size = new System.Drawing.Size(228, 22);
            this.toolStripMenuItemCheckBash.Text = "�&������� ��� �� ���������";
            this.toolStripMenuItemCheckBash.Click += new System.EventHandler(this.toolStripMenuItemCheckBash_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(225, 6);
            // 
            // SaveToolStripMenuItem
            // 
            this.SaveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("SaveToolStripMenuItem.Image")));
            this.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem";
            this.SaveToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.SaveToolStripMenuItem.Text = "�&��������";
            this.SaveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ExitToolStripMenuItem.Image")));
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.ExitToolStripMenuItem.Text = "�&����";
            this.ExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // ���������ToolStripMenuItem
            // 
            this.���������ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SetupToolStripMenuItem,
            this.toolStripMenuItemAutoExec});
            this.���������ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("���������ToolStripMenuItem.Image")));
            this.���������ToolStripMenuItem.Name = "���������ToolStripMenuItem";
            this.���������ToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.���������ToolStripMenuItem.Text = "�&��������";
            // 
            // SetupToolStripMenuItem
            // 
            this.SetupToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("SetupToolStripMenuItem.Image")));
            this.SetupToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.SetupToolStripMenuItem.Name = "SetupToolStripMenuItem";
            this.SetupToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.SetupToolStripMenuItem.Text = "����&�����";
            this.SetupToolStripMenuItem.Click += new System.EventHandler(this.SetupToolStripMenuItem_Click);
            // 
            // toolStripMenuItemAutoExec
            // 
            this.toolStripMenuItemAutoExec.CheckOnClick = true;
            this.toolStripMenuItemAutoExec.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemAutoExec.Image")));
            this.toolStripMenuItemAutoExec.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.toolStripMenuItemAutoExec.Name = "toolStripMenuItemAutoExec";
            this.toolStripMenuItemAutoExec.Size = new System.Drawing.Size(167, 22);
            this.toolStripMenuItemAutoExec.Text = "� ������������!";
            this.toolStripMenuItemAutoExec.Click += new System.EventHandler(this.ToolStripMenuItemAutoExec_Click);
            // 
            // AboutToolStripMenuItem
            // 
            this.AboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AuthorToolStripMenuItem,
            this.VersionsToolStripMenuItem});
            this.AboutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("AboutToolStripMenuItem.Image")));
            this.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem";
            this.AboutToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
            this.AboutToolStripMenuItem.Text = "� &���������";
            // 
            // AuthorToolStripMenuItem
            // 
            this.AuthorToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("AuthorToolStripMenuItem.Image")));
            this.AuthorToolStripMenuItem.Name = "AuthorToolStripMenuItem";
            this.AuthorToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.AuthorToolStripMenuItem.Text = "&���������";
            this.AuthorToolStripMenuItem.Click += new System.EventHandler(this.AuthorToolStripMenuItem_Click);
            // 
            // VersionsToolStripMenuItem
            // 
            this.VersionsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("VersionsToolStripMenuItem.Image")));
            this.VersionsToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.VersionsToolStripMenuItem.Name = "VersionsToolStripMenuItem";
            this.VersionsToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.VersionsToolStripMenuItem.Text = "���&���";
            this.VersionsToolStripMenuItem.Click += new System.EventHandler(this.VersionsToolStripMenuItem_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.ContextMenuStrip = this.contextMenuTray;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "����������� 1.0.3";
            this.notifyIcon.Visible = true;
            this.notifyIcon.BalloonTipClicked += new System.EventHandler(this.notifyIcon_BalloonTipClicked);
            this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
            // 
            // timerGetBash
            // 
            this.timerGetBash.Enabled = true;
            this.timerGetBash.Interval = 100000;
            this.timerGetBash.Tick += new System.EventHandler(this.timerGetBash_Tick);
            // 
            // timerPushRandom
            // 
            this.timerPushRandom.Enabled = true;
            this.timerPushRandom.Interval = 10000;
            this.timerPushRandom.Tick += new System.EventHandler(this.timerPushRandom_Tick);
            // 
            // timerShowEffect
            // 
            this.timerShowEffect.Interval = 50;
            this.timerShowEffect.Tick += new System.EventHandler(this.timerShowEffect_Tick);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 444);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(644, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Image = global::BashReader.Properties.Resources.OK;
            this.statusLabel.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(53, 17);
            this.statusLabel.Text = "�����";
            // 
            // backgroundWorkerInitBash
            // 
            this.backgroundWorkerInitBash.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerInitBash_DoWork);
            this.backgroundWorkerInitBash.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerInitBash_RunWorkerCompleted);
            // 
            // backgroundWorkerCheckBash
            // 
            this.backgroundWorkerCheckBash.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerCheckBash_DoWork);
            this.backgroundWorkerCheckBash.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerCheckBash_RunWorkerCompleted);
            // 
            // backgroundWorkerShowEffect
            // 
            this.backgroundWorkerShowEffect.WorkerSupportsCancellation = true;
            this.backgroundWorkerShowEffect.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerShowEffect_DoWork);
            // 
            // richTextBoxOutput
            // 
            this.richTextBoxOutput.ContextMenuStrip = this.contextMenu;
            this.richTextBoxOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxOutput.HiglightColor = UserControls.RtfColor.White;
            this.richTextBoxOutput.Location = new System.Drawing.Point(3, 16);
            this.richTextBoxOutput.Name = "richTextBoxOutput";
            this.richTextBoxOutput.ReadOnly = true;
            this.richTextBoxOutput.Size = new System.Drawing.Size(638, 401);
            this.richTextBoxOutput.TabIndex = 2;
            this.richTextBoxOutput.Text = "";
            this.richTextBoxOutput.TextColor = UserControls.RtfColor.Black;
            this.richTextBoxOutput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.richTextBoxOutput_KeyDown);
            this.richTextBoxOutput.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.richTextBoxOutput_MouseWheel);
            this.richTextBoxOutput.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.richTextBoxOutput_MouseDoubleClick);
            this.richTextBoxOutput.KeyUp += new System.Windows.Forms.KeyEventHandler(this.richTextBoxOutput_KeyUp);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 466);
            this.Controls.Add(this.JokesViewPanel);
            this.Controls.Add(this.statusStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Opacity = 0.1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.JokesViewPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.contextMenuTray.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel JokesViewPanel;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemHide;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemShowAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ContextMenuStrip contextMenuTray;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemShowForm;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExitApp;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemShowRandom;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLast;
        private System.Windows.Forms.Timer timerPushRandom;
        public System.Windows.Forms.Timer timerGetBash;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSetup;
        private System.Windows.Forms.Timer timerShowEffect;
        private UserControls.RichTextBoxEx richTextBoxOutput;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSaveToFile;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ����ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ���������ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCheckBash;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.ComponentModel.BackgroundWorker backgroundWorkerInitBash;
        private System.ComponentModel.BackgroundWorker backgroundWorkerCheckBash;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAutoExec;
        private System.Windows.Forms.ToolStripMenuItem AuthorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem VersionsToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorkerShowEffect;
    }
}

