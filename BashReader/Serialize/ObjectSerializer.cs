using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using LogLib;

namespace Serialize
{
    class ObjectSerializer
    {
        public static bool SaveObjectToFile(object obj, string filename)
        {
            bool fOk = false;
            try
            {
              
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None);
                formatter.Serialize(stream, obj);
                stream.Close();
                fOk = true;
            }
            catch (Exception e)
            {
                Logger.WriteError(e);
            }
            
            return fOk;
        }
        public static object LoadObjectFromFile(string filename){
            object obj = null;
            try
            {
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                object objFromFile = formatter.Deserialize(stream);
                stream.Close();

                obj = objFromFile;
            }
            catch (Exception e){
                Logger.WriteError(e);
            }

            return obj;
        }

        public static bool LoadConfigFromFile(ref Serialize.ConfigItem obj, string filename)
        {
            bool fOk = false;
         
            object objl = LoadObjectFromFile(filename);
            
            if (objl != null)
            {
                obj = (Serialize.ConfigItem)objl;
                fOk = true;
            }

            return fOk;
        }
    }
}
