using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using BashReader;
namespace Serialize
{
    [Serializable]
    class ConfigItem
    {
       
        private string address = "http://bash.org.ru/rss";
        
        private int intervalOfBashChecking = 100000;
        private int intervalOfRandowShowing = 100000;
        public Proxy Proxy = new Proxy();
        private bool enableShowRandom = true;
        private bool enableCheckBash = true;

#region Properties
        
       
        public string Address
        {
            get { return address; }
            set { address = value; }
        }

       
        public bool EnableCheckBash
        {
            get { return enableCheckBash; }
            set { enableCheckBash = value; }
        }
        public bool EnableShowRandom
        {
            get { return enableShowRandom; }
            set { enableShowRandom = value; }
        }
        public int IntervalOfRandowShowing
        {
            get { return intervalOfRandowShowing; }
            set { intervalOfRandowShowing = value < 1000 ? 1000 : value; }
        }
        public int IntervalOfBashChecking
        {
            get { return intervalOfBashChecking; }
            set { intervalOfBashChecking = value < 10000 ? 10000 : value; }
        }
#endregion
    }
}
