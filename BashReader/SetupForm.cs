using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LogLib;
using System.Net;
using System.IO;

namespace BashReader
{
    public partial class SetupForm : Form
    {
        private string lastUrl = BashConfigReader.Config.Address;
        public SetupForm()
        {
            InitializeComponent();
        }



        private void trackBarGetBash_Scroll(object sender, EventArgs e)
        {
            label3.Text = trackBarGetBash.Value.ToString() + " ���";
            if (trackBarGetBash.Value > 60)
            {
                label3.Text += ", " + Convert.ToDecimal(trackBarGetBash.Value / 60F).ToString() + " ���";
            }
            if (trackBarGetBash.Value > 3600)
            {
                label3.Text += ", " + Convert.ToDecimal(trackBarGetBash.Value / 3600F).ToString() + " �";
            }
        }

        private void SetupForm_Load(object sender, EventArgs e)
        {
            /* IntervalOfBashChecking;
             IntervalOfRandowShowing;
             EnableShowRandom = true;
             EnableCheckBash = true;      
        */
            try
            {
                textBox1.Text = BashConfigReader.Config.Address;
                trackBarGetBash.Value = BashConfigReader.Config.IntervalOfBashChecking / 1000;
                label3.Text = trackBarGetBash.Value.ToString() + " ���";
                trackBarRandom.Value = BashConfigReader.Config.IntervalOfRandowShowing / 1000;
                label4.Text = trackBarRandom.Value.ToString() + " ���";

                checkBoxCheckBash.Checked = BashConfigReader.Config.EnableCheckBash;
                trackBarGetBash.Enabled = checkBoxCheckBash.Checked;

                checkBoxShowRandom.Checked = BashConfigReader.Config.EnableShowRandom;
                trackBarRandom.Enabled = checkBoxShowRandom.Checked;

                
                if (BashConfigReader.Config.Proxy.Using == true) {
                    checkBoxProxy.Checked = true;
                    textBoxProxyIP.Text = BashConfigReader.Config.Proxy.Address.Split(':')[0];
                    textBoxProxyPort.Text = BashConfigReader.Config.Proxy.Address.Split(':')[1];
                    if(BashConfigReader.Config.Proxy.IsAuth){
                        textBoxProxyUser.Text = BashConfigReader.Config.Proxy.Login;
                        textBoxProxyPassword.Text = BashConfigReader.Config.Proxy.Password;
                        checkBoxAuthoraized.Checked = true;
                    }
                } 
                
                trackBarGetBash_Scroll(sender, e);
                trackBarRandom_Scroll(sender, e);

            }
            catch (Exception ee)
            {
                Logger.WriteError(ee);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveChanges();
            Close();
        }
        private void SaveChanges()
        {
            try
            {

                if ((lastUrl == textBox1.Text || checkBoxProxy.Checked) && CheckProxyConnect() == false) 
                    {
                        MessageBox.Show("���������� �� ��������. �������� ������������ ��������� ������ (����� �������, ������)", "���", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        return;
                    }
                
                BashConfigReader.Config.Address = textBox1.Text;

                BashConfigReader.Config.IntervalOfBashChecking = (trackBarGetBash.Value * 1000);
                BashConfigReader.Config.IntervalOfRandowShowing = (trackBarRandom.Value * 1000);
                BashConfigReader.Config.EnableCheckBash = checkBoxCheckBash.Checked;
                BashConfigReader.Config.EnableShowRandom = checkBoxShowRandom.Checked;
                BashConfigReader.Config.Proxy.Using = checkBoxProxy.Checked;
                BashConfigReader.Config.Proxy.Address = textBoxProxyIP.Text + ':' + textBoxProxyPort.Text;
                BashConfigReader.Config.Proxy.IsAuth = checkBoxAuthoraized.Checked;
                BashConfigReader.Config.Proxy.Login = textBoxProxyUser.Text;
                BashConfigReader.Config.Proxy.Password = textBoxProxyPassword.Text;
                
                BashConfigReader.SaveConfigValues();
            }
            catch (Exception e)
            {
                Logger.WriteError(e);
            }
        }

        private bool CheckProxyConnect()
        {
            bool fOk = false;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(textBox1.Text);
                if (BashConfigReader.Config.Proxy.Using == true)
                {
                    WebProxy wp = new WebProxy();
                    wp.Address = new Uri(BashConfigReader.Config.Proxy.Address);

                    if (BashConfigReader.Config.Proxy.IsAuth)
                    {
                        wp.Credentials = new NetworkCredential(BashConfigReader.Config.Proxy.Login,
                         BashConfigReader.Config.Proxy.Password);
                        request.Proxy = wp;
                    }
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1251);
                StreamReader reader = new StreamReader(response.GetResponseStream(), enc);
                reader.ReadToEnd();
                reader.Close();
                response.Close();
                fOk = true;
            }
            catch { }
            return fOk;
        }

        private void checkBoxShowRandom_CheckedChanged(object sender, EventArgs e)
        {
            trackBarRandom.Enabled = checkBoxShowRandom.Checked;
        }

        private void trackBarRandom_Scroll(object sender, EventArgs e)
        {
            label4.Text = trackBarRandom.Value.ToString() + " ���";
            if(trackBarRandom.Value > 60){
                label4.Text += ", " +Convert.ToDecimal(trackBarRandom.Value / 60F).ToString() + " ���";
            }
            if(trackBarRandom.Value > 3600){
                label4.Text += ", " + Convert.ToDecimal(trackBarRandom.Value / 3600F).ToString() + " �";
            }
        }

        private void checkBoxCheckBash_CheckedChanged(object sender, EventArgs e)
        {
            trackBarGetBash.Enabled = checkBoxCheckBash.Checked;
        }

        private void SetupForm_Shown(object sender, EventArgs e)
        {

        }

        private void checkBoxProxy_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxProxy.Enabled = !groupBoxProxy.Enabled;
        }

        private void checkBoxAuthoraized_CheckedChanged(object sender, EventArgs e)
        {
            textBoxProxyPassword.Enabled = !textBoxProxyPassword.Enabled;
            textBoxProxyUser.Enabled = !textBoxProxyUser.Enabled;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}