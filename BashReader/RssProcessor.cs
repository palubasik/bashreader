using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Collections;
using System.Windows.Forms;
using LogLib;

namespace RssProcessing
{
    /*<guid isPermaLink="false">4c4658140a17c13df767a8e99a668e08f2987c389d968e452692e6d6952ecc51</guid> 
			<link>http://bash.org.ru/quote/402469</link> 
			<title>������ #402469</title> 
			<pubDate>Wed, 11 Feb 2009 09:12:01 +0400</pubDate> 
			<description>*/
    struct RssNode
    {
        public string guid;
        public string link;
        public string pubDate;
        public string strDescription;
    };

    struct RssHead
    {
        public string title;
        public string link;
        public string strDescription;
        public string language;
    };

    class RssProcessor
    {
            
        private RssHead rssHead;
        private ArrayList arrRssDataNodes;
        XmlDocument xmlInputDoc;

        public RssProcessor(XmlDocument InputDoc){
            xmlInputDoc = InputDoc;
        }
        public RssProcessor(){

        }
        public bool ProcessRss(){
            
            bool bOk = false;
            
            if(xmlInputDoc != null){
                ProcessRss(xmlInputDoc);
                bOk = true;
            }
            return bOk;
        }
        public ArrayList RssDataNodes{
            get{
                return arrRssDataNodes;
            }
            set{}
        }
        public bool ProcessRss(XmlDocument InputDoc){
            
            xmlInputDoc = InputDoc;
            bool bOk = false;

            try
            {
                XmlNode nodeRss = null;

                for (int i = 0; i < xmlInputDoc.ChildNodes.Count; i++)
                {
                    if (xmlInputDoc.ChildNodes[i].Name == RssConstants.Rss)
                    {

                        nodeRss = xmlInputDoc.ChildNodes[i];
                    }

                }

                XmlNode nodeChannel = null;

                for (int i = 0; i < nodeRss.ChildNodes.Count; i++)
                {
                    if (nodeRss.ChildNodes[i].Name == RssConstants.Channel)
                    {
                        nodeChannel = nodeRss.ChildNodes[i];
                    }

                }
                
                rssHead.language = nodeChannel[RssConstants.Language] != null ? nodeChannel[RssConstants.Language].InnerText : RssConstants.Undefined;
                rssHead.link = nodeChannel[RssConstants.Link] != null ? nodeChannel[RssConstants.Link].InnerText : RssConstants.Undefined;
                rssHead.strDescription = nodeChannel[RssConstants.Description] != null ? nodeChannel[RssConstants.Description].InnerText : RssConstants.Undefined;
                rssHead.title = nodeChannel[RssConstants.Title] != null ? nodeChannel[RssConstants.Title].InnerText : RssConstants.Undefined;

                if (arrRssDataNodes == null)
                    arrRssDataNodes = new ArrayList();
                else
                    arrRssDataNodes.Clear();
                foreach (XmlNode rssNode in nodeChannel)
                {
                    if (rssNode.Name != RssConstants.Item)
                        continue;
                    RssNode rssSaveNode = new RssNode ();
                    rssSaveNode.guid = rssNode[RssConstants.Guid] != null ? rssNode[RssConstants.Guid].InnerText : RssConstants.Undefined;
                    rssSaveNode.link = rssNode[RssConstants.Link] != null ? rssNode[RssConstants.Link].InnerText : RssConstants.Undefined; ;
                    rssSaveNode.pubDate = rssNode[RssConstants.PubDate] != null ? rssNode[RssConstants.PubDate].InnerText : RssConstants.Undefined; ;
                    rssSaveNode.strDescription = rssNode[RssConstants.Description] != null ? rssNode[RssConstants.Description].InnerText : RssConstants.Undefined;

                    arrRssDataNodes.Add(rssSaveNode);
                }
                bOk = true;
            }catch(Exception e){
#if DEBUG
                MessageBox.Show(e.ToString());
#endif
                Logger.WriteError(e);
            }

            return bOk;
        }

    }
}
