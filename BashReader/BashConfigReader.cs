using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using LogLib;
using Serialize;
namespace BashReader
{
    static class BashConfigReader
    {
        private static string strFileName;
        static ConfigItem config = null;

        internal static ConfigItem Config
        {
            get { return BashConfigReader.config; }
            set { BashConfigReader.config = value; }
        }

        static BashConfigReader(){
            strFileName = Constants.ExternalFiles.ConfigFile;
            LoadConfigValues();
        }
        public static void LoadConfigValues(){
            try
            {
                Serialize.ObjectSerializer.LoadConfigFromFile(ref config, strFileName);
                if (config == null)
                    config = new ConfigItem();
            }
            catch (Exception e)
            {
                Logger.WriteError(e);
                MessageBox.Show("������ �������� �������. ��������� �������� �����������. ������ ��� ��� ������ ����������. ",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }
        public static void SaveConfigValues(){
            Serialize.ObjectSerializer.SaveObjectToFile(config, strFileName);
        }
        
    }
}
