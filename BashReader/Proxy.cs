﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BashReader
{
    [Serializable]
    class Proxy
    {
        public bool Using = false;
        public string Address = String.Empty;
        public bool IsAuth = false;
        public string Login = String.Empty;
        public string Password = String.Empty;
    }
}
