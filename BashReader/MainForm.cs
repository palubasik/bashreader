 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Xml;
using System.Collections;
using RssProcessing;
using System.Threading;
using IWshRuntimeLibrary;
using LogLib;

namespace BashReader
{

    public partial class MainForm : Form
    {
        private const int LISTBOX_FONT_SCALE = 8;
        private string[,] arr2HtmlSymbolsToChange = new string[,] { 
                                            { "<br>", "\n" }, 
                                            { "&quot;", "\"" }, 
                                            { "&amp;", "&" },
                                            { "&lt;", "<" },
                                            { "&gt;", ">" },
                                            { "&nbsp;", " " } };
        private int nCurrentJoke = 0;
        private bool bCtrlPressed = false;
        private bool bCheckBashClick = false;
        private bool newUpdatesExists = false;
        private bool bShowAll = false;
        private BashTaker bh = new BashTaker();
       
        public MainForm()
        {
            Opacity = Constants.Opacity.OpacityMin;
            InitializeComponent();

        }

        private ArrayList arrJokes;

        private void LoadSetupValues()
        {

            #region Default values set
            IntervalOfBashChecking = 100000;
            IntervalOfRandowShowing = 100000;
            EnableShowRandom = true;
            EnableCheckBash = true;
            #endregion

            try
            {
                IntervalOfBashChecking = BashConfigReader.Config.IntervalOfBashChecking;
                IntervalOfRandowShowing = BashConfigReader.Config.IntervalOfRandowShowing;
                EnableCheckBash = BashConfigReader.Config.EnableCheckBash;
                EnableShowRandom = BashConfigReader.Config.EnableShowRandom;
            }
            catch (Exception e)
            {
                Logger.WriteError(e);
            }
        }
        private void MainForm_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                //contextMenu.Show();
            }
        }

        private void toolStripMenuItemShow_Click(object sender, EventArgs e)
        {
            //timerPushRandom.Enabled = toolStripMenuItemShow.Checked; 
            Hide();
        }

        private void toolStripMenuItemExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ShowAll()
        {
            if (arrJokes == null || arrJokes.Count <= 0) return;
           
            richTextBoxOutput.Clear();
            richTextBoxOutput.Enabled = false;
            foreach (RssNode joke in arrJokes)
            {
                ShowJoke(joke);
            }
            richTextBoxOutput.SelectionStart = 0;
            richTextBoxOutput.Focus();
            richTextBoxOutput.Enabled = true;
        }
        
        private string ReplaceHtmlSymbols(string inp)
        {
            string str = inp;
            for (int i = 0; i < arr2HtmlSymbolsToChange.Length / 2; i++)
            {
                str = str.Replace(arr2HtmlSymbolsToChange[i, 0], arr2HtmlSymbolsToChange[i, 1]);
            }
            return str;
        }

        private void toolStripMenuItemShowAll_Click(object sender, EventArgs e)
        {
            ShowAll();
            bShowAll = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //  LoadJokes();
        }

        private void toolStripMenuItemExitApp_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toolStripMenuItemShow_Click_1(object sender, EventArgs e)
        {
            FormShowWithEffect();
            
            
            if (arrJokes == null || arrJokes.Count < nCurrentJoke || arrJokes.Count <= 0) return;
            richTextBoxOutput.Clear();
            ShowJoke((RssNode)arrJokes[nCurrentJoke]);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            #region Initial populating
            this.notifyIcon.Text = Constants.VersionInfo.APP_NAME_VER;
            this.Text = Constants.VersionInfo.APP_NAME_VER;
            this.Text = Constants.VersionInfo.APP_NAME_VER;
            #endregion
            //timerShowEffect.Enabled = true;
            if(!backgroundWorkerShowEffect.IsBusy)
                backgroundWorkerShowEffect.RunWorkerAsync();
            toolStripMenuItemAutoExec.Checked = System.IO.File.Exists(Constants.AutoExecConstants.SystemPath + Constants.AutoExecConstants.AppName);
            LoadSetupValues();
            richTextBoxOutput.Clear();
            //Thread InitThread = new Thread(InitialPreparingLoading);
            //InitThread.Start();
            backgroundWorkerInitBash.RunWorkerAsync();
            WindowState = FormWindowState.Normal;  
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
            }
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {

            FormShowWithEffect();
            
           // if (arrJokes == null || arrJokes.Count < nCurrentJoke || arrJokes.Count <= 0) return;
           // bShowAll = false;
           // richTextBoxOutput.Text = ""; 
           // ShowJoke((RssNode)arrJokes[nCurrentJoke]);
        }

        private void listBox1_SizeChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_Resize(object sender, EventArgs e)
        {
        }

        private void timerGetBash_Tick(object sender, EventArgs e)
        {
            toolStripMenuItemCheckBash.Enabled = false;
            if(!backgroundWorkerCheckBash.IsBusy)
                backgroundWorkerCheckBash.RunWorkerAsync();
        }

        private void toolStripMenuItemLast_Click(object sender, EventArgs e)
        {
            if (arrJokes == null || arrJokes.Count < nCurrentJoke || arrJokes.Count <= 0) return;
            richTextBoxOutput.Clear();
            ShowJoke((RssNode)arrJokes[0]);
            bShowAll = false;
        }

        private void ShowJoke(RssNode joke)
        {

            /* string[] arrstrFormatedListItems;
             string strListItem = "";
             string strEql = "";
            
             for (int i = 0; i < listBox1.Width / LISTBOX_FONT_SCALE; i++)
                 strEql += "=";

             listBox1.Items.Add(strEql);
             listBox1.Items.Add(joke.pubDate);
             listBox1.Items.Add(strEql);
             string sss = ReplaceHtmlSymbols(joke.strDescription);
             arrstrFormatedListItems = sss.Split('\n');
             foreach (string s in arrstrFormatedListItems)
             {
                 strListItem = s;
                 while (strListItem.Length > listBox1.Width / LISTBOX_FONT_SCALE)
                 {
                     listBox1.Items.Add(strListItem.Substring(0, listBox1.Width / LISTBOX_FONT_SCALE));
                     strListItem = strListItem.Remove(0, listBox1.Width / LISTBOX_FONT_SCALE);
                 }
                 listBox1.Items.Add(strListItem);
             }
           */

            richTextBoxOutput.AppendText("\n");
            richTextBoxOutput.InsertTextAsRtf( joke.pubDate, new Font(FontFamily.GenericMonospace, 8f, FontStyle.Bold), UserControls.RtfColor.Black,
                UserControls.RtfColor.White);
            richTextBoxOutput.AppendText("\n\n");
            richTextBoxOutput.AppendText(ReplaceHtmlSymbols(joke.strDescription) + "\n\n");
        }

        private void toolStripMenuItemShowRandom_Click(object sender, EventArgs e)
        {
            if (toolStripMenuItemShowRandom.Checked == true)
                EnableShowRandom = true;
            else
                EnableShowRandom = false;
        }

        private void timerPushRandom_Tick(object sender, EventArgs e)
        {
            if (Visible == true) return;
            if (arrJokes == null || arrJokes.Count <= 0) return;
            Random r = new Random((int)DateTime.Now.Ticks);
            double rand = (double)r.Next() / (double)int.MaxValue;
            nCurrentJoke = Convert.ToInt32(Convert.ToDouble(arrJokes.Count - 1) * rand);
            RssNode last = (RssNode)arrJokes[nCurrentJoke];

            string str = ReplaceHtmlSymbols(last.strDescription);
            notifyIcon.BalloonTipText = str;
            
            notifyIcon.BalloonTipTitle = "";
            notifyIcon.BalloonTipIcon = ToolTipIcon.None;
            notifyIcon.ShowBalloonTip(1);
        }

        private void notifyIcon_BalloonTipClicked(object sender, EventArgs e)
        {
            FormShowWithEffect();

            if (arrJokes == null || arrJokes.Count < nCurrentJoke || arrJokes.Count <= 0) return;
            bShowAll = false;
            richTextBoxOutput.Clear();
            ShowJoke((RssNode)arrJokes[nCurrentJoke]);
        }

        private void toolStripMenuItemSetup_Click(object sender, EventArgs e)
        {
            SetupForm sf = new SetupForm();
            sf.ShowDialog();
            LoadSetupValues();
        }

        private void timerShowEffect_Tick(object sender, EventArgs e)
        {
            if (Opacity + Constants.Opacity.OpacityDelta > 1)
            {
                Opacity = Constants.Opacity.OpacityMax;
                timerShowEffect.Enabled = false;
            }
            else
                this.Opacity += Constants.Opacity.OpacityDelta;
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {

        }
        private void FormShowWithEffect()
        {
            Opacity = Constants.Opacity.OpacityMin;
            Show();
            WindowState = FormWindowState.Normal;
            if (!backgroundWorkerShowEffect.IsBusy)
                backgroundWorkerShowEffect.RunWorkerAsync();
        }

        private void richTextBoxOutput_MouseDoubleClick(object sender, MouseEventArgs e)
        {
        }
        private void SaveBashToFile(){
            saveFileDialog1.Filter = "Rich text files (*.rtf)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    richTextBoxOutput.SaveFile(saveFileDialog1.FileName);
                }
                catch (Exception ee)
                {
                    Logger.WriteError(ee);
                    MessageBox.Show("������ ����������!" + ee.ToString(),
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void toolStripMenuItemSaveToFile_Click(object sender, EventArgs e)
        {
            if (richTextBoxOutput.SelectedText == String.Empty)
                SaveBashToFile();
            else
                Clipboard.SetText(richTextBoxOutput.SelectedText);
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveBashToFile();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void SetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetupForm sf = new SetupForm();
            sf.ShowDialog();
            LoadSetupValues();
        }
        private void richTextBoxOutput_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (bCtrlPressed == false) return;
            if(e.Delta > 0){
                if (richTextBoxOutput.ZoomFactor < Constants.ZoomFactor.ZoomMax )
                    richTextBoxOutput.ZoomFactor += Constants.ZoomFactor.ZoomDelta;
            }else{
                if (richTextBoxOutput.ZoomFactor > Constants.ZoomFactor.ZoomMin )
                    richTextBoxOutput.ZoomFactor -= Constants.ZoomFactor.ZoomDelta;
            }
        }

        private void richTextBoxOutput_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Control){
                bCtrlPressed = true;
            }
        }

        private void richTextBoxOutput_KeyUp(object sender, KeyEventArgs e)
        {
            bCtrlPressed = false;
        }

        private void toolStripMenuItemCheckBash_Click(object sender, EventArgs e)
        {
            toolStripMenuItemCheckBash.Enabled = false;
            
            if (!backgroundWorkerCheckBash.IsBusy)
            {
                bCheckBashClick = true;
                backgroundWorkerCheckBash.RunWorkerAsync();
            }
        }
        private delegate void _SetStatusInfo(Image img, string text);
        private void SetStatusInfo(Image img, string text)
        {
            statusLabel.Image = img;
            statusLabel.Text = text;
        }

        private void backgroundWorkerInitBash_DoWork(object sender, DoWorkEventArgs e)
        {
            toolStripMenuItemCheckBash.Enabled = false;
            this.BeginInvoke(new _SetStatusInfo(SetStatusInfo),Constants.Status.StatusImage.Processing,
                Constants.Status.StatusText.Processing);
            bh.InitBashResponse();
            arrJokes = new ArrayList();
            if (bh.ProcessBash())
            {
                arrJokes = bh.BashJokes;
            }
            this.BeginInvoke(new _SetStatusInfo(SetStatusInfo), Constants.Status.StatusImage.Done,
                Constants.Status.StatusText.Done);
            toolStripMenuItemCheckBash.Enabled = true;
        }

        private void backgroundWorkerInitBash_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
           
            ShowAll();
            
            
        }

        private void backgroundWorkerCheckBash_DoWork(object sender, DoWorkEventArgs e)
        {
           
            statusLabel.Image = Constants.Status.StatusImage.Processing;
            statusLabel.Text = Constants.Status.StatusText.Processing;
            
            if (!bh.GetBashChanges())
            {
                if(bCheckBashClick)
                    MessageBox.Show("���������� ���� ���", "�������� ��������� ������", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }else {
                newUpdatesExists = true;
                arrJokes = bh.BashJokes;
            }
            statusLabel.Image = Constants.Status.StatusImage.Done;
            statusLabel.Text = Constants.Status.StatusText.Done;
        }

        private void backgroundWorkerCheckBash_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (newUpdatesExists == true)
            {
                if (Visible == true)
                    ShowAll();
                else {
                    if (arrJokes != null && arrJokes.Count > 0)
                    {
                        nCurrentJoke = 0;
                        RssNode last = (RssNode)arrJokes[0];

                        string str = ReplaceHtmlSymbols(last.strDescription);
                        notifyIcon.BalloonTipText = str;
                        notifyIcon.BalloonTipTitle = "*NEW*";
                        notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
                        notifyIcon.ShowBalloonTip(1);
                    }
                }
                newUpdatesExists = false;
            }
            toolStripMenuItemCheckBash.Enabled = true;
            bCheckBashClick = false;
        }

        private void ToolStripMenuItemAutoExec_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(Constants.AutoExecConstants.SystemPath + Constants.AutoExecConstants.AppName))
            {
                System.IO.File.Delete(Constants.AutoExecConstants.SystemPath + Constants.AutoExecConstants.AppName);
            }

            if(toolStripMenuItemAutoExec.Checked == false){
                
                return;
            }

            try
            {
                WshShellClass wshShell = new WshShellClass();

                IWshShortcut MyShortcut;

                MyShortcut = (IWshRuntimeLibrary.IWshShortcut)wshShell.CreateShortcut(Constants.AutoExecConstants.SystemPath + Constants.AutoExecConstants.AppName);
                MyShortcut.TargetPath = Application.ExecutablePath;
                MyShortcut.Description = Constants.VersionInfo.APP_NAME_VER;
                MyShortcut.IconLocation = Application.StartupPath + @"\favicon.ico";

                MyShortcut.Save();

            }catch(Exception ee){
                MessageBox.Show("������ ���������� � ������������: " + ee.ToString(), "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Logger.WriteError(ee);
                toolStripMenuItemAutoExec.Checked = false;
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                Hide();
                e.Cancel = true;
            }
        }

        private void contextMenu_Opening(object sender, CancelEventArgs e)
        {
            if (richTextBoxOutput.SelectedText == String.Empty)
            {
                toolStripMenuItemSaveToFile.Text = "���������";
            }
            else
                toolStripMenuItemSaveToFile.Text = "���������� (Ctrl+C)";
        }

        private void AuthorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutForm af = new AboutForm();
            af.ShowDialog();
        }

        private void VersionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(System.IO.File.Exists(Constants.VersionInfo.VersionsInfoFile)){
                try
                {
                    richTextBoxOutput.LoadFile(Constants.VersionInfo.VersionsInfoFile,RichTextBoxStreamType.PlainText);
                }catch{
                    richTextBoxOutput.AppendText("!�������� ��� �������� ����� ������");
                }
            }else{
                richTextBoxOutput.AppendText("!�� ������ ���� ������");
            }
        }

        #region Properties
        public int IntervalOfBashChecking
        {
            get
            {
                return timerGetBash.Interval;
            }
            set
            {
                timerGetBash.Interval = value;
            }
        }
        public int IntervalOfRandowShowing
        {
            get
            {
                return timerPushRandom.Interval;
            }
            set
            {
                timerPushRandom.Interval = value;
            }
        }
        public bool EnableShowRandom
        {
            get
            {
                return timerPushRandom.Enabled;
            }
            set
            {
                toolStripMenuItemShowRandom.Checked = value;
                timerPushRandom.Enabled = value;
            }
        }
        public bool EnableCheckBash
        {
            get
            {
                return timerGetBash.Enabled;
            }
            set
            {
                timerGetBash.Enabled = value;
            }
        }
        #endregion
        delegate void OpacityIncr();
        private void OpacityInc() 
        {
            if (Opacity + Constants.Opacity.OpacityDelta > Constants.Opacity.OpacityMax)
            {
                Opacity = Constants.Opacity.OpacityMax;
                this.backgroundWorkerShowEffect.CancelAsync();
            }
            else
                Opacity += Constants.Opacity.OpacityDelta;
        }
        private void backgroundWorkerShowEffect_DoWork(object sender, DoWorkEventArgs e)
        {
            //bool bPlus = true;
            while (true)
            {
                if (backgroundWorkerShowEffect.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                //  if (bPlus)
                // {
                IAsyncResult ires = this.BeginInvoke(new OpacityIncr(this.OpacityInc));
                ires.AsyncWaitHandle.WaitOne();
                Thread.Sleep(50);
                
                
                /* }else{
                     if (Opacity - Constants.Opacity.OpacityDelta < Constants.Opacity.OpacityMin)
                     {
                         Opacity = Constants.Opacity.OpacityMin;
                         bPlus = true;
                     }
                     else
                         Opacity -= Constants.Opacity.OpacityDelta;
                 }*/
            }
        }
    }
}