namespace BashReader
{
    partial class SetupForm
    { 
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        System.Windows.Forms.TrackBar trackBarGetBash;
        private void InitializeComponent()
        {
            
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.trackBarRandom = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBoxProxy = new System.Windows.Forms.GroupBox();
            this.textBoxProxyIP = new System.Windows.Forms.TextBox();
            this.checkBoxAuthoraized = new System.Windows.Forms.CheckBox();
            this.textBoxProxyPort = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxProxyPassword = new System.Windows.Forms.TextBox();
            this.textBoxProxyUser = new System.Windows.Forms.TextBox();
            this.checkBoxProxy = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.checkBoxShowRandom = new System.Windows.Forms.CheckBox();
            this.checkBoxCheckBash = new System.Windows.Forms.CheckBox();
            trackBarGetBash = new System.Windows.Forms.TrackBar();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(trackBarGetBash)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRandom)).BeginInit();
            this.groupBoxProxy.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBoxProxy);
            this.panel1.Controls.Add(this.checkBoxProxy);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.checkBoxShowRandom);
            this.panel1.Controls.Add(this.checkBoxCheckBash);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(442, 547);
            this.panel1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(417, 61);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "������ ����";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(13, 24);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(181, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(trackBarGetBash);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 100);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(417, 77);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            // 
            // trackBarGetBash
            // 
            trackBarGetBash.Enabled = false;
            trackBarGetBash.LargeChange = 1;
            trackBarGetBash.Location = new System.Drawing.Point(6, 26);
            trackBarGetBash.Maximum = 10000;
            trackBarGetBash.Minimum = 100;
            trackBarGetBash.Name = "trackBarGetBash";
            trackBarGetBash.Size = new System.Drawing.Size(396, 45);
            trackBarGetBash.TabIndex = 0;
            trackBarGetBash.TickFrequency = 100;
            trackBarGetBash.TickStyle = System.Windows.Forms.TickStyle.Both;
            trackBarGetBash.Value = 100;
            trackBarGetBash.Scroll += new System.EventHandler(this.trackBarGetBash_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "����� ������:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(95, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "���";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.trackBarRandom);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 206);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(417, 89);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            // 
            // trackBarRandom
            // 
            this.trackBarRandom.Enabled = false;
            this.trackBarRandom.LargeChange = 1;
            this.trackBarRandom.Location = new System.Drawing.Point(13, 32);
            this.trackBarRandom.Maximum = 1000;
            this.trackBarRandom.Minimum = 1;
            this.trackBarRandom.Name = "trackBarRandom";
            this.trackBarRandom.Size = new System.Drawing.Size(389, 45);
            this.trackBarRandom.TabIndex = 1;
            this.trackBarRandom.TickFrequency = 100;
            this.trackBarRandom.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBarRandom.Value = 1;
            this.trackBarRandom.Scroll += new System.EventHandler(this.trackBarRandom_Scroll);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(200, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "����� ����������� ��������� �����:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(216, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "���";
            // 
            // groupBoxProxy
            // 
            this.groupBoxProxy.Controls.Add(this.textBoxProxyIP);
            this.groupBoxProxy.Controls.Add(this.checkBoxAuthoraized);
            this.groupBoxProxy.Controls.Add(this.textBoxProxyPort);
            this.groupBoxProxy.Controls.Add(this.label8);
            this.groupBoxProxy.Controls.Add(this.label5);
            this.groupBoxProxy.Controls.Add(this.label7);
            this.groupBoxProxy.Controls.Add(this.label6);
            this.groupBoxProxy.Controls.Add(this.textBoxProxyPassword);
            this.groupBoxProxy.Controls.Add(this.textBoxProxyUser);
            this.groupBoxProxy.Enabled = false;
            this.groupBoxProxy.Location = new System.Drawing.Point(12, 324);
            this.groupBoxProxy.Name = "groupBoxProxy";
            this.groupBoxProxy.Size = new System.Drawing.Size(417, 159);
            this.groupBoxProxy.TabIndex = 20;
            this.groupBoxProxy.TabStop = false;
            this.groupBoxProxy.Text = "������";
            // 
            // textBoxProxyIP
            // 
            this.textBoxProxyIP.Location = new System.Drawing.Point(157, 19);
            this.textBoxProxyIP.Name = "textBoxProxyIP";
            this.textBoxProxyIP.Size = new System.Drawing.Size(150, 20);
            this.textBoxProxyIP.TabIndex = 11;
            // 
            // checkBoxAuthoraized
            // 
            this.checkBoxAuthoraized.AutoSize = true;
            this.checkBoxAuthoraized.Location = new System.Drawing.Point(21, 52);
            this.checkBoxAuthoraized.Name = "checkBoxAuthoraized";
            this.checkBoxAuthoraized.Size = new System.Drawing.Size(106, 17);
            this.checkBoxAuthoraized.TabIndex = 19;
            this.checkBoxAuthoraized.Text = "� ������������";
            this.checkBoxAuthoraized.UseVisualStyleBackColor = true;
            this.checkBoxAuthoraized.CheckedChanged += new System.EventHandler(this.checkBoxAuthoraized_CheckedChanged);
            // 
            // textBoxProxyPort
            // 
            this.textBoxProxyPort.Location = new System.Drawing.Point(330, 19);
            this.textBoxProxyPort.Name = "textBoxProxyPort";
            this.textBoxProxyPort.Size = new System.Drawing.Size(65, 20);
            this.textBoxProxyPort.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(36, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "������:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(313, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = ":";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "��� ������������:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "�����:";
            // 
            // textBoxProxyPassword
            // 
            this.textBoxProxyPassword.Enabled = false;
            this.textBoxProxyPassword.Location = new System.Drawing.Point(157, 107);
            this.textBoxProxyPassword.Name = "textBoxProxyPassword";
            this.textBoxProxyPassword.Size = new System.Drawing.Size(238, 20);
            this.textBoxProxyPassword.TabIndex = 16;
            this.textBoxProxyPassword.UseSystemPasswordChar = true;
            // 
            // textBoxProxyUser
            // 
            this.textBoxProxyUser.Enabled = false;
            this.textBoxProxyUser.Location = new System.Drawing.Point(157, 81);
            this.textBoxProxyUser.Name = "textBoxProxyUser";
            this.textBoxProxyUser.Size = new System.Drawing.Size(238, 20);
            this.textBoxProxyUser.TabIndex = 15;
            // 
            // checkBoxProxy
            // 
            this.checkBoxProxy.AutoSize = true;
            this.checkBoxProxy.Location = new System.Drawing.Point(12, 301);
            this.checkBoxProxy.Name = "checkBoxProxy";
            this.checkBoxProxy.Size = new System.Drawing.Size(138, 17);
            this.checkBoxProxy.TabIndex = 10;
            this.checkBoxProxy.Text = "������������ ������";
            this.checkBoxProxy.UseVisualStyleBackColor = true;
            this.checkBoxProxy.CheckedChanged += new System.EventHandler(this.checkBoxProxy_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(11, 501);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(418, 26);
            this.button2.TabIndex = 7;
            this.button2.Text = "���������";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // checkBoxShowRandom
            // 
            this.checkBoxShowRandom.AutoSize = true;
            this.checkBoxShowRandom.Location = new System.Drawing.Point(12, 183);
            this.checkBoxShowRandom.Name = "checkBoxShowRandom";
            this.checkBoxShowRandom.Size = new System.Drawing.Size(137, 17);
            this.checkBoxShowRandom.TabIndex = 5;
            this.checkBoxShowRandom.Text = "���������� ��������";
            this.checkBoxShowRandom.UseVisualStyleBackColor = true;
            this.checkBoxShowRandom.CheckedChanged += new System.EventHandler(this.checkBoxShowRandom_CheckedChanged);
            // 
            // checkBoxCheckBash
            // 
            this.checkBoxCheckBash.AutoSize = true;
            this.checkBoxCheckBash.Location = new System.Drawing.Point(12, 79);
            this.checkBoxCheckBash.Name = "checkBoxCheckBash";
            this.checkBoxCheckBash.Size = new System.Drawing.Size(112, 17);
            this.checkBoxCheckBash.TabIndex = 4;
            this.checkBoxCheckBash.Text = "���������� ���";
            this.checkBoxCheckBash.UseVisualStyleBackColor = true;
            this.checkBoxCheckBash.CheckedChanged += new System.EventHandler(this.checkBoxCheckBash_CheckedChanged);
            // 
            // SetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(442, 547);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SetupForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "���������";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.SetupForm_Load);
            this.Shown += new System.EventHandler(this.SetupForm_Shown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(trackBarGetBash)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRandom)).EndInit();
            this.groupBoxProxy.ResumeLayout(false);
            this.groupBoxProxy.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TrackBar trackBarRandom;
        private System.Windows.Forms.CheckBox checkBoxShowRandom;
        private System.Windows.Forms.CheckBox checkBoxCheckBash;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxProxyPassword;
        private System.Windows.Forms.TextBox textBoxProxyUser;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxProxyPort;
        private System.Windows.Forms.TextBox textBoxProxyIP;
        private System.Windows.Forms.CheckBox checkBoxProxy;
        private System.Windows.Forms.CheckBox checkBoxAuthoraized;
        private System.Windows.Forms.GroupBox groupBoxProxy;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}