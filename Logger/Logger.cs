﻿using System;
using System.Collections.Generic;

using System.Text;
using System.IO;
using System.Net;
using System.Configuration;

namespace LogLib
{
    public enum LoggerMessageType 
    { 
        DEBUG,
        EVENT,
        ERROR
    }
    public class Logger
    {
        protected static string strLogFilePath = string.Empty;

        private static string base_path = AppDomain.CurrentDomain.BaseDirectory;
        private static string error_file = String.Empty;
        private static string event_file = String.Empty;
        private static string debug_event_file = String.Empty;
        private static string debug_lev2_event_file = String.Empty;
        private static object IOErrorLocker = new object();
        private static object IOEventLocker = new object();
        private static object IODebugEventLocker = new object();

        private static bool bEnableErrorLog = true;
        private static bool bEnableDebugLog = true;
        private static bool bEnableWarningLog = true;

        private static long MaxLogFileSize = Constants.Logger.MaxLogFileSize;
        static Logger()
        {
            if (!bool.TryParse(ConfigurationSettings.AppSettings["EnableErrorLog"], out bEnableErrorLog))
            {
                bEnableErrorLog = true;
            }

            if (!bool.TryParse(ConfigurationSettings.AppSettings["EnableDebugLog"], out bEnableDebugLog))
            {
                bEnableDebugLog = true;
            }

            if (!bool.TryParse(ConfigurationSettings.AppSettings["EnableWarningLog"], out bEnableWarningLog))
            {
                bEnableWarningLog = true;
            }

            if (!long.TryParse(ConfigurationSettings.AppSettings["MaxLogFileSize"], out MaxLogFileSize))
            {
                MaxLogFileSize = Constants.Logger.MaxLogFileSize;
            }
            error_file = base_path + String.Format("{0}_{1}.txt", "ErrorLog", DateTime.Now.ToString("MM_dd_yyyy"));
            event_file = base_path + String.Format("{0}_{1}.txt", "EventLog", DateTime.Now.ToString("MM_dd_yyyy"));
            debug_event_file = base_path + String.Format("{0}_{1}.txt", "DebugLog", DateTime.Now.ToString("MM_dd_yyyy"));
        }

        public static bool WriteError(Exception objException)
        {
            bool bRes = false;

            if (!bEnableErrorLog)
                return bRes;


            bRes = WriteErrorToFile(objException);

            return bRes;
        }

        public static bool WriteEvent(string message) 
        {
            return WriteEvent(message, LoggerMessageType.EVENT,null);
        }

        public static bool WriteEvent(string message,Exception exc)
        {
            return WriteEvent(message, LoggerMessageType.EVENT, exc);
        }

        public static bool WriteDebugEvent(string message)
        {
            return WriteDebugEvent(message, null);
        }
      
        public static bool WriteDebugEvent(string message, Exception objException)
        {
            bool bReturn = false;
            if (bEnableDebugLog == false)
                return bReturn;

            lock (IODebugEventLocker)
            {

                try
                {
                    bool bNewCreated = false;

                    if (File.Exists(debug_event_file) == false || (new FileInfo(debug_event_file)).Length > MaxLogFileSize)
                    {
                        using (FileStream fs = File.Create(debug_event_file))
                        {
                            fs.Close();
                        }
                        bNewCreated = true;
                    }

                    using (StreamWriter sw = new StreamWriter(debug_event_file, true))
                    {
                        if (bNewCreated)
                            sw.WriteLine(String.Format("Start at {0} =====================================================", DateTime.Now));

                        sw.WriteLine(String.Format("[{0}] {1}", DateTime.Now, message));
                        if (objException != null)
                        {
                            sw.WriteLine("Error		: " + (objException.Message != null ? objException.Message.ToString().Trim() : "null"));
                            sw.WriteLine("Stack Trace	: " + (objException.StackTrace != null ? objException.StackTrace.ToString().Trim() : "null"));
                        }
                        sw.Flush();
                        sw.Close();
                    }
                    bReturn = true;
                }
                catch (Exception)
                {
                    bReturn = false;
                }
            }
            return bReturn;
        }

        public static bool WriteEvent(string message, LoggerMessageType type, Exception objException)
        {
            bool bReturn = false;
            if (bEnableWarningLog == false)
                return bReturn;

            lock (IOEventLocker)
            {
               
                try
                {
                    bool bNewCreated = false;

                    if (File.Exists(event_file) == false || (new FileInfo(event_file)).Length > MaxLogFileSize)
                    {
                        using (FileStream fs = File.Create(event_file))
                        {
                            fs.Close();
                        }
                        bNewCreated = true;
                    }

                    using (StreamWriter sw = new StreamWriter(event_file, true))
                    {
                        if (bNewCreated)
                            sw.WriteLine(String.Format("Start at {0} =====================================================", DateTime.Now));

                        sw.WriteLine(String.Format("[{0}] - {1} : {2}", DateTime.Now, type.ToString(), message));
                        if (objException != null)
                        {
                            sw.WriteLine("Error		: " + (objException.Message != null ? objException.Message.ToString().Trim() : "null"));
                            sw.WriteLine("Stack Trace	: " + (objException.StackTrace != null ? objException.StackTrace.ToString().Trim() : "null"));
                        }
                        sw.Flush();
                        sw.Close();
                    }
                    bReturn = true;
                }
                catch (Exception)
                {
                    bReturn = false;
                }
            }
            return bReturn;
        }
        private static bool WriteErrorToFile(Exception objException)
        {
            bool bReturn = false;
            string strException = string.Empty;
            lock (IOErrorLocker)
            {
                try
                {
                    bool bNewCreated = false;

                    if (File.Exists(error_file) == false || (new FileInfo(error_file)).Length > MaxLogFileSize)
                    {
                        using (FileStream fs = File.Create(error_file))
                        {
                            fs.Close();
                        }
                        bNewCreated = true;
                    }


                    using (StreamWriter sw = new StreamWriter(error_file, true))
                    {
                        if (bNewCreated)
                            sw.WriteLine(String.Format("Start at {0} =====================================================", DateTime.Now));
                        sw.WriteLine("EventType     : " + LoggerMessageType.ERROR.ToString());
                        sw.WriteLine("Source		: " + (objException.Source != null ? objException.Source.ToString().Trim() : "null"));
                        sw.WriteLine("Method		: " + (objException.TargetSite != null ? objException.TargetSite.Name.ToString() : "null"));
                        sw.WriteLine("Date		: " + DateTime.Now.ToLongTimeString());
                        sw.WriteLine("Time		: " + DateTime.Now.ToShortDateString());
                        sw.WriteLine("Computer	: " + Dns.GetHostName().ToString());
                        sw.WriteLine("Error		: " + (objException.Message != null ? objException.Message.ToString().Trim() : "null"));
                        sw.WriteLine("Stack Trace	: " + (objException.StackTrace != null ? objException.StackTrace.ToString().Trim() : "null"));
                        sw.WriteLine("==========================================================================");
                        sw.Flush();
                        sw.Close();
                    }
                    bReturn = true;
                }
                catch (Exception)
                {
                    bReturn = false;
                }
            }
            return bReturn;
        }
    }
}


